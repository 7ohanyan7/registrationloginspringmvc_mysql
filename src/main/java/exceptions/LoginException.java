package exceptions;

public class LoginException extends RuntimeException {
    public LoginException(String exception){
        super(exception);
    }
}
