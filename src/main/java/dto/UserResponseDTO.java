package dto;

import entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserResponseDTO {

    private Long id;
    private String name;
    private String login;
    private String email;

    public UserResponseDTO(Long id, String name, String login, String email) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public static User mapDtoToEntity(UserResponseDTO userResponseDTO) {

        User user = new User(
                userResponseDTO.getId(),
                userResponseDTO.getName(),
                userResponseDTO.getLogin(),
                userResponseDTO.getEmail());

        return user;
    }

    public static List<User> mapDtosToEntities(List<UserResponseDTO> userResponseDTOS) {

        List<User> users = new ArrayList<>();

        for (UserResponseDTO userResponseDTO : userResponseDTOS) {
            users.add(mapDtoToEntity(userResponseDTO));
        }

        return users;
    }

    public static UserResponseDTO mapEntityToDto(User user) {

        UserResponseDTO userResponseDTO = new UserResponseDTO(
                user.getId(),
                user.getName(),
                user.getLogin(),
                user.getEmail());

        return userResponseDTO;
    }

    public static List<UserResponseDTO> mapEntitiesToDto(List<User> users) {

        List<UserResponseDTO> userResponseDTOS = new ArrayList<>();

        for (User user : users) {
            userResponseDTOS.add(mapEntityToDto(user));
        }

        return userResponseDTOS;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
