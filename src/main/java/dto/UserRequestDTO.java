package dto;

import entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRequestDTO {
    private String name;
    private String login;
    private String email;
    private String password;
    private String confirmPassword;

    public UserRequestDTO(String name, String login, String email, String password, String confirmPassword) {
        this.name = name;
        this.login = login;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public UserRequestDTO(String name, String login, String email, String password) {
        this.name = name;
        this.login = login;
        this.email = email;
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public static User mapDtoToEntity(UserRequestDTO userRequestDTO) {

        User user = new User(
                userRequestDTO.getName(),
                userRequestDTO.getLogin(),
                userRequestDTO.getEmail(),
                userRequestDTO.getPassword());

        return user;
    }

    public static List<User> mapDtosToEntities(List<UserRequestDTO> userRequestDTOS) {

        List<User> users = new ArrayList<>();

        for (UserRequestDTO userDto : userRequestDTOS) {
            users.add(mapDtoToEntity(userDto));
        }

        return users;
    }

    public static UserRequestDTO mapEntityToDto(User user) {

        UserRequestDTO userRequestDTO = new UserRequestDTO(
                user.getName(),
                user.getLogin(),
                user.getEmail(),
                user.getPassword());

        return userRequestDTO;
    }

    public static List<UserRequestDTO> mapEntitiesToDto(List<User> users) {

        List<UserRequestDTO> userRequestDTOS = new ArrayList<>();

        for (User user : users) {
            userRequestDTOS.add(mapEntityToDto(user));
        }

        return userRequestDTOS;
    }


    @Override
    public String toString() {
        return "UserRequestDTO{" +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
