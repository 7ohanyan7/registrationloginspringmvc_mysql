package service;

import dto.UserRequestDTO;
import dto.UserResponseDTO;
import entity.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {

    UserResponseDTO save(UserRequestDTO userRequestDTO);

    UserResponseDTO findByID(Long id);

    UserResponseDTO findByLogin(String login);

    List<UserResponseDTO> findAll();

    UserResponseDTO register(UserRequestDTO userRequestDTO);

    UserResponseDTO login(String login, String password);

}
