package service;

import dto.UserRequestDTO;
import dto.UserResponseDTO;
import entity.User;
import exceptions.RegistrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.DataBase;
import repository.UserRepository;
import validator.LoginValidator;
import validator.RegistrationValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private LoginValidator loginValidator;
    private RegistrationValidator registrationValidator;

    public UserServiceImpl() {
    }

    @Autowired
    public UserServiceImpl(UserRepository userRepository, LoginValidator loginValidator, RegistrationValidator registrationValidator) {
        this.userRepository = userRepository;
        this.loginValidator = loginValidator;
        this.registrationValidator = registrationValidator;
    }

    public UserResponseDTO save(UserRequestDTO userRequestDTO) {
        return UserResponseDTO.mapEntityToDto(userRepository.save(UserRequestDTO.mapDtoToEntity(userRequestDTO)));
    }

    public UserResponseDTO findByID(Long id) {
        return UserResponseDTO.mapEntityToDto(userRepository.findByID(id));
    }

    public UserResponseDTO findByLogin(String login) {
        return UserResponseDTO.mapEntityToDto(userRepository.findByLogin(login));
    }

    public List<UserResponseDTO> findAll() {
        return UserResponseDTO.mapEntitiesToDto(userRepository.findAll());
    }

    public UserResponseDTO register(UserRequestDTO userRequestDTO) {

        registrationValidator.validate(userRequestDTO, userRepository);

        return save(userRequestDTO);

    }

    public UserResponseDTO login(String login, String password) {

        loginValidator.validate(login, password, userRepository);


        return findByLogin(login);

    }


}
