package repository;

import dto.UserResponseDTO;
import entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private JdbcTemplate jdbcTemplate;

    public UserRepositoryImpl() {
    }

    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public User save(User user) {
//        Long id = ((Integer)(DataBase.getUsers().size() + 1)).longValue();
//        user.setId(id);
//        DataBase.getUsers().add(user);
//        return user;
        jdbcTemplate.update("INSERT INTO users (name, login, email, password) VALUES (?, ?, ?, ?)",
                user.getName(),
                user.getLogin(),
                user.getEmail(),
                user.getPassword());

        return (User) jdbcTemplate.queryForObject("SELECT * FROM users where login = ? ",
                new Object[] { user.getLogin() }, new BeanPropertyRowMapper(User.class));

    }

    public User findByID(Long id) {
//        for (User user : DataBase.getUsers()){
//            if (user.getId().equals(id))
//                return user;
//        }
//        return null;
        User user = null;
        try {
            user = (User) jdbcTemplate.queryForObject("SELECT * FROM users where id = ? ",
                    new Object[] { id }, new BeanPropertyRowMapper(User.class));
        }catch (EmptyResultDataAccessException ex){
            return null;
        }


        return user;
    }

    public User findByLogin(String login) {
//        for (User user : DataBase.getUsers()){
//            if (user.getLogin().equals(login))
//                return user;
//        }
//        return null;

        User user = null;
        try {
            user = (User) jdbcTemplate.queryForObject("SELECT * FROM users where login = ? ",
                    new Object[] { login }, new BeanPropertyRowMapper(User.class));
        }catch (EmptyResultDataAccessException ex){
            return null;
        }


        return user;

    }

    public List<User> findAll() {
//        return DataBase.getUsers();
        return jdbcTemplate.query("SELECT * FROM users", new BeanPropertyRowMapper(User.class));
    }
}
