package repository;


import dto.UserResponseDTO;
import entity.User;

import java.util.List;

public interface UserRepository {
    User save(User user);

    User findByID(Long id);

    User findByLogin(String login);

    List<User> findAll();
}
