package repository;

import entity.User;

import java.util.ArrayList;
import java.util.List;

public class DataBase {
    private static List<User> userList;

    private DataBase () {}

    public static List<User> getUsers() {
        if (userList == null) {
            userList = new ArrayList<User>();
        }
        return userList;
    }


}
