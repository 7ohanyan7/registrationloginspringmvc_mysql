package validator;

import dto.UserRequestDTO;
import dto.UserResponseDTO;
import entity.User;
import exceptions.LoginException;
import org.springframework.stereotype.Component;
import repository.DataBase;
import repository.UserRepository;

@Component
public class LoginValidator {

    private UserRepository userRepository;

    public LoginValidator() {
    }

    public void validate(String login, String password, UserRepository userRepository) {
        this.userRepository = userRepository;
        passwordMatch(loginCheck(login),password);
    }

    private UserRequestDTO loginCheck(String login){
        if (userRepository.findByLogin(login) != null)
        {
            return UserRequestDTO.mapEntityToDto(userRepository.findByLogin(login));
        }
        throw new LoginException("Invalid login");
    }

    private void passwordMatch(UserRequestDTO userRequestDTO, String password){
        if (!userRequestDTO.getPassword().equals(password))
        {
            throw new LoginException("Incorrect password");
        }
    }
}
