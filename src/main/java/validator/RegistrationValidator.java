package validator;

import dto.UserRequestDTO;
import entity.User;
import exceptions.RegistrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.DataBase;
import repository.UserRepository;
import service.UserService;

@Component
public class RegistrationValidator {

    private UserRepository userRepository;

    public RegistrationValidator() {
    }


    public void validate(UserRequestDTO userRequestDTO, UserRepository userRepository) {
        this.userRepository = userRepository;
        validateEmail(userRequestDTO.getEmail());
        validateLogin(userRequestDTO.getLogin());
    }

    private void validateEmail(String email){
        for (User user: userRepository.findAll())
        {
            if (UserRequestDTO.mapEntityToDto(user).getEmail().equals(email))
            {
                throw new RegistrationException("Email already registered");
            }
        }
    }

    private void validatePassword(String password, String repeatPassword){
        if (!password.equals(repeatPassword))
        {
            throw new RegistrationException("Passwords not match");
        }
    }


    private void validateLogin(String login){
        if (userRepository.findByLogin(login) != null)
        {
            throw new RegistrationException("Login already registered");
        }
    }

    public void validateInputs(UserRequestDTO userRequestDTO){
        if (userRequestDTO.getName().length() != 0 &&
                userRequestDTO.getEmail().length() != 0 &&
                userRequestDTO.getLogin().length() != 0 &&
                userRequestDTO.getPassword().length() != 0 &&
                userRequestDTO.getConfirmPassword().length() != 0)
        {
            validatePassword(userRequestDTO.getPassword(), userRequestDTO.getConfirmPassword());
        } else {
            throw new RegistrationException("Complete all inputs");
        }
    }
}
