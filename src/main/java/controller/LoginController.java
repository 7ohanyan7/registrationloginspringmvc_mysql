package controller;

import dto.UserResponseDTO;
import entity.User;
import exceptions.LoginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/login")
    public String login(@RequestParam(name = "login") String login,@RequestParam(name = "password") String password, Model model, HttpServletRequest request){


        try {
            UserResponseDTO userResponseDTO = userService.login(login,password);
            HttpSession session = request.getSession();
            session.setAttribute("id",userResponseDTO.getId());
            model.addAttribute("name",userResponseDTO.getName());
            return "/welcome.jsp";
        }catch (LoginException ex){
            String message = ex.getMessage();
            model.addAttribute("message", message);
            return "/login.jsp";
        }


    }
}
