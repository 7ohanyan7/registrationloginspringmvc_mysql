package controller;

import dto.UserRequestDTO;
import entity.User;
import exceptions.RegistrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.UserService;
import validator.RegistrationValidator;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegistrationController {

    private UserService userService;
    private RegistrationValidator registrationValidator;

    @Autowired
    public RegistrationController(UserService userService, RegistrationValidator registrationValidator) {
        this.userService = userService;
        this.registrationValidator = registrationValidator;
    }

    @RequestMapping(value = "/registration",method = RequestMethod.POST)
    public String register(HttpServletRequest request, Model model){

        String name = request.getParameter("name");
        String login = request.getParameter("login");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        String message = null;

        try {
            UserRequestDTO userRequestDTO = new UserRequestDTO(name,login,email,password,confirmPassword);
            registrationValidator.validateInputs(userRequestDTO);
            userService.register(userRequestDTO);
            message = "Successful registration";
        }catch (RegistrationException ex){
            message = ex.getMessage();
        }

        model.addAttribute("message",message);


        return "/registration.jsp";
    }
}
