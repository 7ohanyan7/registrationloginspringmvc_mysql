package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LogoutController {

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request){

        HttpSession session = request.getSession(false);
        session.invalidate();

        return "/index.jsp";
    }
}
