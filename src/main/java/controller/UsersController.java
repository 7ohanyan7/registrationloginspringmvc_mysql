package controller;

import dto.UserResponseDTO;
import entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UsersController {
    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users")
    public String users(Model model){

        model.addAttribute("users",userService.findAll());

        return "/users.jsp";
    }

    @RequestMapping(value = "/users/{id}")
    public String user(@PathVariable Long id, Model model){
        UserResponseDTO userResponseDTO = userService.findByID(id);

        model.addAttribute("user",userResponseDTO);
        return "/user.jsp";
    }

    @RequestMapping(value = "/details")
    public String details(HttpServletRequest request, ModelMap modelMap){

        HttpSession session = request.getSession(false);
        Long id = (Long)session.getAttribute("id");
        UserResponseDTO userResponseDTO = userService.findByID(id);
        modelMap.addAttribute("login",userResponseDTO.getLogin());
        modelMap.addAttribute("email",userResponseDTO.getEmail());
        return "/details.jsp";
    }
}
